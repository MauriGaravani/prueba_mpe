#include <Arduino.h>
#include <interrupt_isr.h>

void setup() {
  pinMode(ENTRADA_555, INPUT);
  pinMode(LED, OUTPUT);
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(ENTRADA_555), interrupt_isr, RISING);
  }

void loop() {
  digitalWrite(LED, estado);
  pulsos = pulseIn(ENTRADA_555, HIGH);
  duracion = (pulsos/100000);
  Serial.println(duracion);
}